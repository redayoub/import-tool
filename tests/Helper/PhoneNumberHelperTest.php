<?php

namespace Tests\Helper;

use App\Helper\PhoneNumberHelper;
use PHPUnit\Framework\TestCase;

class PhoneNumberHelperTest extends TestCase
{
    /**
     * Test is valid.
     */
    public function testIsValid()
    {
        $this->assertTrue(PhoneNumberHelper::isValid('+1-541-754-3010', 'US'));
        $this->assertFalse(PhoneNumberHelper::isValid('+21277998508', 'US'));
    }
}
