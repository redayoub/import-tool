<?php

namespace Tests\Helper;

use App\Helper\EmailHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EmailHelperTest extends TestCase
{
    /**
     * Test is valid.
     */
    public function testIsValid()
    {
        $mockValidator = $this->createMock(ValidatorInterface::class);
        $mockValidator
            ->expects($this->at(0))
            ->method('validate')
            ->will($this->returnValue([]));
        $mockValidator
            ->expects($this->at(1))
            ->method('validate')
            ->will($this->returnValue(['']));

        $emailHelper = new EmailHelper($mockValidator);
        $this->assertTrue($emailHelper->isValid('user@email.com'));
        $this->assertFalse($emailHelper->isValid('useremail.com'));
    }
}
