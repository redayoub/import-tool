<?php

namespace App\Controller;

use ApiPlatform\Core\Api\UrlGeneratorInterface;
use App\Entity\Import;
use App\Message\Command\ImportFileCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class DefaultController extends AbstractController
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var MessageBusInterface */
    protected $commandBus;

    /**
     * Constructor.
     */
    public function __construct(
        EntityManagerInterface $em,
        MessageBusInterface $commandBus,
        Breadcrumbs $breadcrumbs,
        UrlGeneratorInterface $router
    ) {
        $this->em = $em;
        $this->commandBus = $commandBus;
        
        $this->breadcrumbs = $breadcrumbs;
        $this->breadcrumbs
            ->addItem('Home', $router->generate('homepage'))
            ->addItem('Imports', $router->generate('imports'));
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->redirectToRoute('imports');
    }

    /**
     * @Route("/imports", name="imports")
     */
    public function imports()
    {
        $this->commandBus->dispatch(new ImportFileCommand(18));

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/imports/new", name="import_new")
     */
    public function newImport(Request $request, SluggerInterface $slugger)
    {
        if ($request->isMethod('POST')) {
            if ($file = $request->files->get('file')) {
                if ($file->getClientOriginalExtension() != 'csv') {
                    die('Please upload a CSV file');
                }

                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$file->getClientOriginalExtension();

                try {
                    $file->move(
                        $this->getParameter('kernel.project_dir') . '/public/uploads',
                        $newFilename
                    );

                    $import = new Import();
                    $import
                        ->setCreatedById(1)
                        ->setClientId(1)
                        ->setFilename($newFilename);
                    
                    $this->em->persist($import);
                    $this->em->flush();
    
                    $this->addFlash('success', 'New import added');

                    $this->commandBus->dispatch(new ImportFileCommand($import->getId()));
    
                    return $this->redirectToRoute('imports');
                } catch (FileException $e) {
                    $this->addFlash('error', 'Upload failed');
    
                    return $this->redirectToRoute('imports');
                }
            }
        }

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/imports/{id}", name="import_view")
     */
    public function import(Import $import)
    {
        $this->breadcrumbs->addItem('#' . $import->getId());

        return $this->render('default/import.html.twig', [
            'import' => $import
        ]);
    }
}
