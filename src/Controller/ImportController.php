<?php

namespace App\Controller;

use App\Helper\EmailHelper;
use App\Helper\PhoneNumberHelper;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    /** @var EmailHelper */
    protected $emailHelper;

    /** @var ParameterBagInterface */
    protected $parameterBag;

    /**
     * Constructor.
     */
    public function __construct(
        EmailHelper $emailHelper,
        ParameterBagInterface $parameterBag
    ) {
        $this->emailHelper = $emailHelper;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @Route("/import", name="import")
     */
    public function index()
    {
        $results = [];

        $csv = Reader::createFromPath($this->parameterBag->get('kernel.project_dir').'/data.csv');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());

        $records = $stmt->process($csv);
        foreach ($records as $record) {
            $isPhoneValid = PhoneNumberHelper::isValid($record['phone'], strtoupper($record['country']));
            $isEmailValid = $this->emailHelper->isValid($record['email'], true);

            $results[] = [
                'data' => sprintf('%s,%s,%s,%s', $record['name'], $record['email'], $record['phone'], $record['country']),
                'isValidEmail' => $isEmailValid,
                'isValidPhoneNumber' => $isPhoneValid,
            ];
        }

        return $this->json($results);
    }
}
