<?php

namespace App\Message\Command;

class ImportFileCommand
{
    /** @var integer */
    private $fileId;

    /**
     * Constructor
     */
    public function __construct(int $fileId)
    {
        $this->fileId = $fileId;
    }

    /**
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     */
    public function setFileId($fileId): void
    {
        $this->fileId = $fileId;
    }
}