<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ImportDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ImportDataRepository::class)
 */
class ImportData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $recordId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nickname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $middleName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $suffix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $maiden;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $socialClassYear;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secondaryEmail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mobilePhone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $workPhone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $homePhone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $preferredPhone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $website;

    /**
     * @ORM\Column(type="text")
     */
    private $homeAddress1;

    /**
     * @ORM\Column(type="text")
     */
    private $homeAddress2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $homeCity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $homeState;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $homeZip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $homeCountry;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $careerPosition;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currentEmployer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $careerPastPositions;

    /**
     * @ORM\Column(type="text")
     */
    private $workAddress1;

    /**
     * @ORM\Column(type="text")
     */
    private $workAddress2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $workCity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $workState;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $workZip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $workCountry;

    /**
     * @ORM\Column(type="text")
     */
    private $facebookLink;

    /**
     * @ORM\Column(type="text")
     */
    private $linkedinLink;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $twitter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dobYear;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dobMonth;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dobDay;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a1Field1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a1Years;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a2Field1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a2Years;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a2Field2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a3Field1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a3Years;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a3Field2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tags;

    /**
     * @ORM\Column(type="text")
     */
    private $adminNotes;

    /**
     * @ORM\Column(type="array")
     */
    private $customFields = [];

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Import::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $import;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $a1Field2;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecordId(): ?string
    {
        return $this->recordId;
    }

    public function setRecordId(string $recordId): self
    {
        $this->recordId = $recordId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    public function setSuffix(string $suffix): self
    {
        $this->suffix = $suffix;

        return $this;
    }

    public function getMaiden(): ?string
    {
        return $this->maiden;
    }

    public function setMaiden(string $maiden): self
    {
        $this->maiden = $maiden;

        return $this;
    }

    public function getSocialClassYear(): ?string
    {
        return $this->socialClassYear;
    }

    public function setSocialClassYear(string $socialClassYear): self
    {
        $this->socialClassYear = $socialClassYear;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSecondaryEmail(): ?string
    {
        return $this->secondaryEmail;
    }

    public function setSecondaryEmail(string $secondaryEmail): self
    {
        $this->secondaryEmail = $secondaryEmail;

        return $this;
    }

    public function getBio(): ?string
    {
        return $this->bio;
    }

    public function setBio(string $bio): self
    {
        $this->bio = $bio;

        return $this;
    }

    public function getMobilePhone(): ?string
    {
        return $this->mobilePhone;
    }

    public function setMobilePhone(string $mobilePhone): self
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    public function getWorkPhone(): ?string
    {
        return $this->workPhone;
    }

    public function setWorkPhone(string $workPhone): self
    {
        $this->workPhone = $workPhone;

        return $this;
    }

    public function getHomePhone(): ?string
    {
        return $this->homePhone;
    }

    public function setHomePhone(string $homePhone): self
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    public function getPreferredPhone(): ?string
    {
        return $this->preferredPhone;
    }

    public function setPreferredPhone(string $preferredPhone): self
    {
        $this->preferredPhone = $preferredPhone;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getHomeAddress1(): ?string
    {
        return $this->homeAddress1;
    }

    public function setHomeAddress1(string $homeAddress1): self
    {
        $this->homeAddress1 = $homeAddress1;

        return $this;
    }

    public function getHomeAddress2(): ?string
    {
        return $this->homeAddress2;
    }

    public function setHomeAddress2(string $homeAddress2): self
    {
        $this->homeAddress2 = $homeAddress2;

        return $this;
    }

    public function getHomeCity(): ?string
    {
        return $this->homeCity;
    }

    public function setHomeCity(string $homeCity): self
    {
        $this->homeCity = $homeCity;

        return $this;
    }

    public function getHomeState(): ?string
    {
        return $this->homeState;
    }

    public function setHomeState(string $homeState): self
    {
        $this->homeState = $homeState;

        return $this;
    }

    public function getHomeZip(): ?string
    {
        return $this->homeZip;
    }

    public function setHomeZip(string $homeZip): self
    {
        $this->homeZip = $homeZip;

        return $this;
    }

    public function getHomeCountry(): ?string
    {
        return $this->homeCountry;
    }

    public function setHomeCountry(string $homeCountry): self
    {
        $this->homeCountry = $homeCountry;

        return $this;
    }

    public function getCareerPosition(): ?string
    {
        return $this->careerPosition;
    }

    public function setCareerPosition(string $careerPosition): self
    {
        $this->careerPosition = $careerPosition;

        return $this;
    }

    public function getCurrentEmployer(): ?string
    {
        return $this->currentEmployer;
    }

    public function setCurrentEmployer(string $currentEmployer): self
    {
        $this->currentEmployer = $currentEmployer;

        return $this;
    }

    public function getCareerPastPositions(): ?string
    {
        return $this->careerPastPositions;
    }

    public function setCareerPastPositions(string $careerPastPositions): self
    {
        $this->careerPastPositions = $careerPastPositions;

        return $this;
    }

    public function getWorkAddress1(): ?string
    {
        return $this->workAddress1;
    }

    public function setWorkAddress1(string $workAddress1): self
    {
        $this->workAddress1 = $workAddress1;

        return $this;
    }

    public function getWorkAddress2(): ?string
    {
        return $this->workAddress2;
    }

    public function setWorkAddress2(string $workAddress2): self
    {
        $this->workAddress2 = $workAddress2;

        return $this;
    }

    public function getWorkCity(): ?string
    {
        return $this->workCity;
    }

    public function setWorkCity(string $workCity): self
    {
        $this->workCity = $workCity;

        return $this;
    }

    public function getWorkState(): ?string
    {
        return $this->workState;
    }

    public function setWorkState(string $workState): self
    {
        $this->workState = $workState;

        return $this;
    }

    public function getWorkZip(): ?string
    {
        return $this->workZip;
    }

    public function setWorkZip(string $workZip): self
    {
        $this->workZip = $workZip;

        return $this;
    }

    public function getWorkCountry(): ?string
    {
        return $this->workCountry;
    }

    public function setWorkCountry(string $workCountry): self
    {
        $this->workCountry = $workCountry;

        return $this;
    }

    public function getFacebookLink(): ?string
    {
        return $this->facebookLink;
    }

    public function setFacebookLink(string $facebookLink): self
    {
        $this->facebookLink = $facebookLink;

        return $this;
    }

    public function getLinkedinLink(): ?string
    {
        return $this->linkedinLink;
    }

    public function setLinkedinLink(string $linkedinLink): self
    {
        $this->linkedinLink = $linkedinLink;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getDobYear(): ?string
    {
        return $this->dobYear;
    }

    public function setDobYear(string $dobYear): self
    {
        $this->dobYear = $dobYear;

        return $this;
    }

    public function getDobMonth(): ?string
    {
        return $this->dobMonth;
    }

    public function setDobMonth(string $dobMonth): self
    {
        $this->dobMonth = $dobMonth;

        return $this;
    }

    public function getDobDay(): ?string
    {
        return $this->dobDay;
    }

    public function setDobDay(string $dobDay): self
    {
        $this->dobDay = $dobDay;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getA1Years(): ?string
    {
        return $this->a1Years;
    }

    public function setA1Years(string $a1Years): self
    {
        $this->a1Years = $a1Years;

        return $this;
    }

    public function getA2Field1(): ?string
    {
        return $this->a2Field1;
    }

    public function setA2Field1(string $a2Field1): self
    {
        $this->a2Field1 = $a2Field1;

        return $this;
    }

    public function getA2Years(): ?string
    {
        return $this->a2Years;
    }

    public function setA2Years(string $a2Years): self
    {
        $this->a2Years = $a2Years;

        return $this;
    }

    public function getA2Field2(): ?string
    {
        return $this->a2Field2;
    }

    public function setA2Field2(string $a2Field2): self
    {
        $this->a2Field2 = $a2Field2;

        return $this;
    }

    public function getA3Field1(): ?string
    {
        return $this->a3Field1;
    }

    public function setA3Field1(string $a3Field1): self
    {
        $this->a3Field1 = $a3Field1;

        return $this;
    }

    public function getA3Years(): ?string
    {
        return $this->a3Years;
    }

    public function setA3Years(string $a3Years): self
    {
        $this->a3Years = $a3Years;

        return $this;
    }

    public function getA3Field2(): ?string
    {
        return $this->a3Field2;
    }

    public function setA3Field2(string $a3Field2): self
    {
        $this->a3Field2 = $a3Field2;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(string $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getAdminNotes(): ?string
    {
        return $this->adminNotes;
    }

    public function setAdminNotes(string $adminNotes): self
    {
        $this->adminNotes = $adminNotes;

        return $this;
    }

    public function getCustomFields(): ?array
    {
        return $this->customFields;
    }

    public function setCustomFields(array $customFields): self
    {
        $this->customFields = $customFields;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getA1Field1(): ?string
    {
        return $this->a1Field1;
    }

    public function setA1Field1(string $a1Field1): self
    {
        $this->a1Field1 = $a1Field1;

        return $this;
    }

    public function getImport(): ?Import
    {
        return $this->import;
    }

    public function setImport(?Import $import): self
    {
        $this->import = $import;

        return $this;
    }

    public function getA1Field2(): ?string
    {
        return $this->a1Field2;
    }

    public function setA1Field2(string $a1Field2): self
    {
        $this->a1Field2 = $a1Field2;

        return $this;
    }
}
