<?php

namespace App\MessageHandler\Command;

use App\Entity\ImportData;
use League\Csv\Reader;
use League\Csv\Statement;
use Psr\Log\LoggerInterface;
use App\Repository\ImportRepository;
use App\Message\Command\ImportFileCommand;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ImportFileCommandHandler implements MessageHandlerInterface
{
    /** @var LoggerInterface */
    private $logger;

    /** @var MessageBusInterface */
    private $commandBus;

    /** @var ImportRepository */
    private $importRepository;

    /** @var ParameterBagInterface */
    private $parameterBag;

    /**
     * Constructor
     *
     * @param LoggerInterface $messengerLogger
     * @param MessageBusInterface $commandBus
     * @param ImportRepository $importRepository
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        LoggerInterface $messengerLogger,
        MessageBusInterface $commandBus,
        ImportRepository $importRepository,
        ParameterBagInterface $parameterBag
    ) {
        $this->logger = $messengerLogger;
        $this->commandBus = $commandBus;
        $this->importRepository = $importRepository;
        $this->parameterBag = $parameterBag;
    }

    /**
     * Invoke
     *
     * @param ImportFileCommand $importFileCommand
     */
    public function __invoke(ImportFileCommand $importFileCommand)
    {
        $import = $this->importRepository->find($importFileCommand->getFileId());
        if (!$import) {
            return;
        }

        $csv = Reader::createFromPath($this->parameterBag->get('kernel.project_dir').'/public/uploads/'.$import->getFilename());
        #$csv = Reader::createFromPath($this->parameterBag->get('kernel.project_dir').'/data.csv');
        $headers = $csv->fetchOne();
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());

        $records = $stmt->process($csv);
        foreach ($records as $record) {
            $importData = new ImportData();
            //$this->logger->info(json_encode($record));
            $customFields = [];

            foreach ($record as $key => $value) {    
                $setter = 'set' . str_replace([' ', '(', ')'], '', ucwords(strtolower($key)));
    
                if (method_exists($importData, $setter)) {
                    call_user_func_array([$importData, $setter], [$value]);
                }

                if (strstr($key, 'Custom Field')) {
                    $customFields[] = [
                        'key' => $key,
                        'value' => $value
                    ];
                }
            }

            $importData
                ->setCustomFields($customFields)
                ->setImport($import);
            $this->importRepository->persist($importData);
        }

        $this->importRepository->flush();
    }
}
