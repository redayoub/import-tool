<?php

namespace App\Helper;

use libphonenumber\PhoneNumberUtil;
use Exception;

class PhoneNumberHelper
{
    /**
     * Check if phone number is valid.
     */
    public static function isValid(string $phoneNumber, string $country): bool
    {
        if (!$phoneNumber) {
            return false;
        }

        try {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $parsedPhoneNumber = $phoneUtil->parse($phoneNumber, $country);
            $isValid = $phoneUtil->isValidNumber($parsedPhoneNumber);

            return $isValid;
        } catch (Exception $e) {
            return false;
        }
    }
}
