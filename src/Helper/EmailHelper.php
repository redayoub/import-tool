<?php

namespace App\Helper;

use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EmailHelper
{
    /** @var ValidatorInterface */
    protected $validator;

    /** @var ParameterBagInterface */
    protected $parameterBag;

    /**
     * Constructor.
     *
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        ValidatorInterface $validator,
        ParameterBagInterface $parameterBag = null
    ) {
        $this->validator = $validator;
        $this->parameterBag = $parameterBag;
    }

    /**
     * Check if email is valid.
     */
    public function isValid(string $email, bool $zerobounceValidation = false): bool
    {
        $emailConstraint = new Assert\Email();
        $errors = $this->validator->validate(
            $email,
            $emailConstraint
        );

        if (count($errors)) {
            return false;
        }

        if ($zerobounceValidation) {
            try {
                $client = new Client();
                $response = $client->request('GET', 'https://api.zerobounce.net/v2/validate',
                    [
                        'query' => [
                            'api_key' => $this->parameterBag->get('zerobounce_api_key'),
                            'email' => $email,
                            'ip_address' => '',
                        ],
                    ]
                );
                $responseData = json_decode($response->getBody()->getContents(), true);

                return 'valid' == $responseData['status'];
            } catch (\Exception $e) {
                echo $e->getMessage();

                return false;
            }
        }

        return true;
    }
}
